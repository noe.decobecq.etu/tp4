import View from './View.js';

export class AboutView extends View {
	constructor(element) {
		super(element);
	}

	show() {
		fetch('./about.html')
			.then(response => response.text())
			.then(responseText => this.showFileContent(responseText));
	}

	showFileContent(html) {
		this.element.innerHTML = html;
		super.show();
	}
}
